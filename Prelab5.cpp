#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <vector>
using namespace std;
class Filereader
{
public:
	string no1;
	string no2;
	string no3;
	string no4;
	string no5;

	int x;
	int y;
	string m;
	char op;
	int id1;
	int id2;
	char op2;
	int id12;
	int id22;
	char op3;
	int id13;
	int id23;
	void readnums() {
		ifstream file;
		file.open("input.txt");
		file >> x;
		//cout << "numbers:" << x<<endl;
		file >> y;
		//cout << "digits:" << y << endl;
		getline(file, m);
		getline(file, no1);
		//cout << "no1:" << no1 <<endl;
		file >> y;
		getline(file, m);
		//cout << "digits:" << y << endl;
		getline(file, no2);
		//cout << "no2:" << no2 << endl;
		if (x > 2) {
			file >> y;
			getline(file, m);
			//cout << "digits:" << y << endl;
			getline(file, no3);
			//cout << "no3:" << no3 << endl;
		}
		if (x > 3) {
			getline(file, m);
			//cout << "digits:" << y << endl;
			getline(file, no4);
			//cout << "no4:" << no4 << endl;
		}
		if (x > 4) {
			file >> y;
			getline(file, m);
			//cout << "digits:" << y << endl;
			getline(file, no5);
			//cout << "no5:" << no5 << endl;
		}
		file >> op;
		file >> id1;
		file >> id2;
		file >> op2;
		file >> id12;
		file >> id22;
		file >> op3;
		file >> id13;
		file >> id23;
		
		//cout << op << id1 << id2 << endl;
	}
private:

};
class multiply
{
public:
	void print(string no1, string no2) {
		num1 = no1;
		num2 = no2;
		cout << "The result of " << no1 << " X " << no2 << " is:" << mult()<<endl;
	}
	string mult()
	{
		int len1 = num1.size();
		int len2 = num2.size();
		if (len1 == 0 || len2 == 0)
			return "0";

		// will keep the result number in vector 
		// in reverse order 
		vector<int> result(len1 + len2, 0);

		// Below two indexes are used to find positions 
		// in result.  
		int i_n1 = 0;
		int i_n2 = 0;

		// Go from right to left in num1 
		for (int i = len1 - 1; i >= 0; i--)
		{
			int carry = 0;
			int n1 = num1[i] - '0';

			// To shift position to left after every 
			// multiplication of a digit in num2 
			i_n2 = 0;

			// Go from right to left in num2              
			for (int j = len2 - 1; j >= 0; j--)
			{
				// Take current digit of second number 
				int n2 = num2[j] - '0';

				// Multiply with current digit of first number 
				// and add result to previously stored result 
				// at current position.  
				int sum = n1 * n2 + result[i_n1 + i_n2] + carry;

				// Carry for next iteration 
				carry = sum / 10;

				// Store result 
				result[i_n1 + i_n2] = sum % 10;

				i_n2++;
			}

			// store carry in next cell 
			if (carry > 0)
				result[i_n1 + i_n2] += carry;

			// To shift position to left after every 
			// multiplication of a digit in num1. 
			i_n1++;
		}

		// ignore '0's from the right 
		int i = result.size() - 1;
		while (i >= 0 && result[i] == 0)
			i--;

		// If all were '0's - means either both or 
		// one of num1 or num2 were '0' 
		if (i == -1)
			return "0";

		// generate the result string 
		string s = "";

		while (i >= 0)
			s += std::to_string(result[i--]);

		return s;
	}


private:
	string num1;
	string num2;
};
class subtraction
{
public:
	void print(string no1, string no2) {
		num1 = no1;
		num2 = no2;
		cout << "The result of " << no1 << " - " << no2 << " is ";
		if (isSmaller(no1, no2)) {
			cout << "-" << Diff() << endl;
		}
		else
		{
			cout << Diff() << endl;
		}
	}
	bool isSmaller(string str1, string str2) 
{ 
    // Calculate lengths of both string 
    int n1 = str1.length(), n2 = str2.length(); 
  
    if (n1 < n2) 
    return true; 
    if (n2 < n1) 
    return false; 
  
    for (int i=0; i<n1; i++) 
    if (str1[i] < str2[i]) 
        return true; 
    else if (str1[i] > str2[i]) 
        return false; 
  
    return false; 
} 
  
// Function for find difference of larger numbers 
string Diff() 
{ 
    // Before proceeding further, make sure str1 
    // is not smaller 
    if (isSmaller(num1, num2)) 
        swap(num1, num2); 
  
    // Take an empty string for storing result 
    string str = ""; 
  
    // Calculate length of both string 
    int n1 = num1.length(), n2 = num2.length(); 
  
    // Reverse both of strings 
    reverse(num1.begin(), num1.end()); 
    reverse(num2.begin(), num2.end()); 
      
    int carry = 0; 
  
    // Run loop till small string length 
    // and subtract digit of str1 to str2 
    for (int i=0; i<n2; i++) 
    { 
        // Do school mathematics, compute difference of 
        // current digits 
          
        int sub = ((num1[i]-'0')-(num2[i]-'0')-carry); 
          
        // If subtraction is less then zero 
        // we add then we add 10 into sub and 
        // take carry as 1 for calculating next step 
        if (sub < 0) 
        { 
            sub = sub + 10; 
            carry = 1; 
        } 
        else
            carry = 0; 
  
        str.push_back(sub + '0'); 
    } 
  
    // subtract remaining digits of larger number 
    for (int i=n2; i<n1; i++) 
    { 
        int sub = ((num1[i]-'0') - carry); 
          
        // if the sub value is -ve, then make it positive 
        if (sub < 0) 
        { 
            sub = sub + 10; 
            carry = 1; 
        } 
        else
            carry = 0; 
              
        str.push_back(sub + '0'); 
    } 
  
    // reverse resultant string 
    reverse(str.begin(), str.end()); 
  
    return str; 
} 


private:
	string num1;
	string num2;
};
class addition
{
public:
	void print(string no1, string no2) {
		num1 = no1;
		num2 = no2;
		cout << "The result of " << no1 << " + " << no2 << " is " << Sum()<<endl;
	}
	string Sum()
	{
		// Before proceeding further, make sure length 
		// of str2 is larger. 
		if (num1.length() > num2.length())
			swap(num1, num2);

		// Take an empty string for storing result 
		string str = "";

		// Calculate length of both string 
		int n1 = num1.length(), n2 = num2.length();

		// Reverse both of strings 
		reverse(num1.begin(), num1.end());
		reverse(num2.begin(), num2.end());

		int carry = 0;
		for (int i = 0; i < n1; i++)
		{
			// Do school mathematics, compute sum of 
			// current digits and carry 
			int sum = ((num1[i] - '0') + (num2[i] - '0') + carry);
			str.push_back(sum % 10 + '0');

			// Calculate carry for next step 
			carry = sum / 10;
		}

		// Add remaining digits of larger number 
		for (int i = n1; i < n2; i++)
		{
			int sum = ((num2[i] - '0') + carry);
			str.push_back(sum % 10 + '0');
			carry = sum / 10;
		}

		// Add remaining carry 
		if (carry)
			str.push_back(carry + '0');

		// reverse resultant string 
		reverse(str.begin(), str.end());

		return str;
	}
private:
	string num1;
	string num2;
};



void setnums(string &no1,string &no2) {
	Filereader read;
	read.readnums();
	if (read.id1 == 1) {
		no1 = read.no1;
	}
	if (read.id1 == 2) {
		no1 = read.no2;
	}
	if (read.id1 == 3) {
		no1 = read.no3;
	}
	if (read.id1 == 4) {
		no1 = read.no4;
	}
	if (read.id1 == 5) {
		no1 = read.no5;
	}
	if (read.id2 == 1) {
		no2 = read.no1;
	}
	if (read.id2 == 2) {
		no2 = read.no2;
	}
	if (read.id2 == 3) {
		no2 = read.no3;
	}
	if (read.id2 == 4) {
		no2 = read.no4;
	}
	if (read.id2 == 5) {
		no2 = read.no5;
	}
}
void setnums2(string& no1, string& no2) {
	Filereader read;
	read.readnums();
	if (read.id12 == 1) {
		no1 = read.no1;
	}
	if (read.id12 == 2) {
		no1 = read.no2;
	}
	if (read.id12 == 3) {
		no1 = read.no3;
	}
	if (read.id12 == 4) {
		no1 = read.no4;
	}
	if (read.id12 == 5) {
		no1 = read.no5;
	}
	if (read.id22 == 1) {
		no2 = read.no1;
	}
	if (read.id22 == 2) {
		no2 = read.no2;
	}
	if (read.id22 == 3) {
		no2 = read.no3;
	}
	if (read.id22 == 4) {
		no2 = read.no4;
	}
	if (read.id22 == 5) {
		no2 = read.no5;
	}
}

void setnums3(string& no1, string& no2) {
	Filereader read;
	read.readnums();
	if (read.id13 == 1) {
		no1 = read.no1;
	}
	if (read.id13 == 2) {
		no1 = read.no2;
	}
	if (read.id13 == 3) {
		no1 = read.no3;
	}
	if (read.id13 == 4) {
		no1 = read.no4;
	}
	if (read.id13 == 5) {
		no1 = read.no5;
	}
	if (read.id23 == 1) {
		no2 = read.no1;
	}
	if (read.id23 == 2) {
		no2 = read.no2;
	}
	if (read.id23 == 3) {
		no2 = read.no3;
	}
	if (read.id23 == 4) {
		no2 = read.no4;
	}
	if (read.id23 == 5) {
		no2 = read.no5;
	}
}

int main() {
	string no1, no2;
	Filereader read;
	multiply multiply;
	addition sum;
	subtraction sub;
	read.readnums();
	setnums(no1, no2);
	if (read.op == 'S') {
		sub.print(no1, no2);
	}
	if (read.op == 'A') {
		sum.print(no1, no2);
	}
	if (read.op == 'M') {
		multiply.print(no1, no2);
	}
	setnums2(no1, no2);
	if (read.op2 == 'S') {
		sub.print(no1, no2);
	}
	if (read.op2 == 'A') {
		sum.print(no1, no2);
	}
	if (read.op2 == 'M') {
		multiply.print(no1, no2);
	}
	setnums3(no1, no2);
	if (read.op3 == 'S') {
		sub.print(no1, no2);
	}
	if (read.op3 == 'A') {
		sum.print(no1, no2);
	}
	if (read.op3 == 'M') {
		multiply.print(no1, no2);
	}
	system("pause");
}



